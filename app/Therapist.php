<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Therapist extends Model
{
    /**
     * @var string
     */
    protected $table  = 'therapists';

    protected $fillable = [
      'name',
      'price',
      'specialization',
      'rate',
    ];

    public static function getListSpecialization(): array
    {
        return  [
          'مشاكل الاطفال',
          'مشاكل المراهقه',
          'الاكتئاب',
          'القلق',
          'المشاكل الجنسيه',
        ];
    }
}
