<?php

namespace App\Http\Controllers;

use App\Therapist;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\View\View;

class TherapistController extends Controller
{
    public function getAll(): View
    {
        $therapists = Therapist::all();
        return view('welcome', compact('therapists'));
    }

    public function filter(Request $request)
    {
        $operator = $request->filterName === 'name' ? 'LIKE' : '=';
        $filter = $request->filterName === 'name' ? "%$request->filter%" : "$request->filter";
        $therapists = $this->filterBy($request->filterName, $filter, $operator);
        $response = view('layouts.parts.filter-result', compact('therapists'))->render();
        return response()->json([
            'data' => $response
        ]);
    }

    public function filterBy(string $key, string $value = null, $operator = '='): Collection
    {
        $therapists = Therapist::query();
        if ($value !== null && $value !== '' && $value !== 'all') {
            $therapists = Therapist::where($key, $operator, $value);
        }
        return $therapists->get();
    }

    public function sort(Request $request)
    {
        $therapists = $this->sortBy($request->sortType);
        $response = view('layouts.parts.filter-result', compact('therapists'))->render();
        return response()->json([
            'data' => $response
        ]);
    }

    public function sortBy(string $sortType): Collection
    {
        if ($sortType === 'rate') {
            return Therapist::orderBy('rate', 'desc')->get();
        }
        return Therapist::orderBy('price', $sortType)->get();
    }
}
