<div class="row hide-stepper" style="margin-right: 0 !important; margin-left: 0 !important;">
    <div class="col-lg-12 col-md-12 col-md-3 stepper-content">
        <div class="mdl-card mdl-shadow--2dp">
            <h1 class="head-content"> 3 خطوات نحو حياة افضل </h1>
            <div class="mdl-card__supporting-text">

                <div class="mdl-stepper-horizontal-alternative">

                    <div class="mdl-stepper-step">
                        <div class="mdl-stepper-circle"><span>أكمل عمليه الدفع</span></div>
                        <div class="mdl-stepper-bar-left"></div>
                        <div class="mdl-stepper-bar-right"></div>
                    </div>

                    <div class="mdl-stepper-step active-step">
                        <div class="mdl-stepper-circle"><span>حدد الوقت والتاريخ</span></div>
                        <div class="mdl-stepper-bar-left"></div>
                        <div class="mdl-stepper-bar-right"></div>
                    </div>
                    <div class="mdl-stepper-step">
                        <div class="mdl-stepper-circle activeStep"><span>اختار المعالج</span></div>
                        <div class="mdl-stepper-bar-left"></div>
                        <div class="mdl-stepper-bar-right"></div>
                    </div>
                </div>

            </div>

        </div>
</div>
</div>
