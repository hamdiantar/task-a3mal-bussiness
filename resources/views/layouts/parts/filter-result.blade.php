@if(count($therapists))
@foreach($therapists as  $therapist)
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card card-1">
            <div class="card-content">
                <div class="product-pic">
                    <img class="pic1" src="https://scontent.shezlong.com/therapist_profile_pictures/ae89e0497eca764eed4f79b99a1a850f.png">
                </div>
                <small class="category">{{$therapist->name}}</small>
                <h5 class="doc-name">معالج نفسي</h5>
                <div class="row px-3 justify-content-between" style="direction: ltr">
                    <p>    (134 تقييم)  {{$therapist->rate}}  </p>
                    <div class="stars">
                        <span class="fa fa-star star-active"></span>
                        <span class="fa fa-star star-active"></span>
                        <span class="fa fa-star star-active"></span>
                        <span class="fa fa-star star-active"></span>
                        <span class="fa fa-star star-active"></span>
                    </div>
                </div>
                <div class="des">
                    <p>
                        متخصص فى اضطراب قلق الفراق، القلق العام، الرهاب الاجتماعي , العلاقات
                    </p>
                </div>
                <div class="actions">
                    <div class="action-right">
                        <p> {{$therapist->price}}  دولار </p>
                        <img src="{{asset('images/dollar.png')}}">
                    </div>

                    <div class="action-left">
                        <p>450+ جلسة</p>
                        <img src="{{asset('images/arrow.png')}}">
                    </div>
                </div>
            </div>
            <div class="buttons">
                <div class="button-right">
                    <button class="btn btn-primary btn-profile" type="button"> الصفحه الشخصيه  </button>
                </div>

                <div class="button-left">
                    <button  data-toggle="modal" data-target="#modal_aside_right" class="btn btn-primary btn-book" type="button"> الحجز الأن  </button>
                </div>
            </div>
        </div>
    </div>
@endforeach
@else
    <div class="col-lg-12 col-md-6 col-sm-12">
        <div class="alert alert-info text-center" style="background-color: #ea9292 !important;">
            <strong>لاتوجد بيانات</strong>
        </div>
        <div
@endif
