<div id="modal_aside_filter" class="modal fixed-right fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-aside" role="document">
        <div class="modal-content" style="overflow-y: auto; direction: rtl">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3" style="margin-top: -13px !important;">
                        <div class="row" style="background-color: #ffffff!important; padding-bottom: 120px !important;">
                            <div class="col-12 bg-info p-2" style="border-radius: 2px">
                                <button class="bg-info border-0 float-left text-white">مسح التصنيف</button>
                                <button class="bg-info border-0 float-right text-white font-weight-bold">التصنيف</button>
                            </div>

                            <div class="col-12 mt-3">
                                <img src="{{asset('images/time.png')}}" alt="" class="rounded float-right" height="30px">
                                <p class="mt-1 float-right">المواعيد المتاحة والمدة</p>
                            </div>

                            <div class="col-12">
                                <div class="filter-2">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineCheckbox2">هذا الاسبوع</label>
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineCheckbox1">اليوم</label>
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="filter-3">
                                    <p>متاح من : اختر ميعاد</p>
                                    <input type="text" class="form-control" placeholder="اختار تاريخ" id="dateFrom" style="direction: rtl;">
                                    <p>متاح الى : اختر ميعاد</p>
                                    <input type="text" class="form-control" placeholder="اختار تاريخ" id="dateTo" style="direction: rtl;">
                                </div>
                            </div>

                            <div class="col-12 mt-1">
                                <p class="mt-1 float-right">:Duration</p>
                            </div>

                            <div class="col-12">
                                <div class="filter-2">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineCheckboxff2">30 min</label>
                                        <input class="form-check-input" type="checkbox" id="inlineCheckboxff2" value="option2">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineCheckddboxdd1">60 min</label>
                                        <input class="form-check-input" type="checkbox" id="inlineCheckddboxdd1" value="option1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mt-3">
                                <img src="{{asset('images/gender.png')}}" alt="" class="rounded float-right" height="30px">
                                <p class="mt-1 float-right">الجنس</p>
                            </div>

                            <div class="col-12">
                                <div class="filter-2">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineCheckbox2">ذكر</label>
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineCheckbox1">انثى</label>
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mt-2">
                                <img src="{{asset('images/rate.png')}}" alt="" class="rounded float-right" height="30px">
                                <p class="mt-1 float-right">التقييم</p>
                            </div>

                            <div class="col-12 mt-1">
                                <div class="stars float-right" style="font-size: 22px">
                                    <span class="fa fa-star star-active"></span>
                                    <span class="fa fa-star star-active"></span>
                                    <span class="fa fa-star star-active"></span>
                                    <span class="fa fa-star star-active"></span>
                                    <span class="fa fa-star star-active"></span>
                                </div>
                                <p class="float-right pr-3 mt-2">وما فوق</p>
                            </div>

                            <div class="col-12 mt-2">
                                <img src="{{asset('images/lang.png')}}" alt="" class="rounded float-right" height="30px">
                                <p class="mt-1 float-right">اللغه</p>
                            </div>

                            <div class="col-12 mt-1">
                                <div class="form-group" style="border: .5px solid #7d7c7c2e;padding: 3px">
                                    <select
                                        class="form-control border-0 bg-light form-control fz16 sm-no-border-radius h-100 no-border search-bar"
                                        id="exampleFormControlSelect1">
                                        <option>جميع التخصصات</option>
                                        <option>مشاكل الاطفال</option>
                                        <option>مشاكل المراهقه</option>
                                        <option>الاكتئاب</option>
                                        <option>القلق</option>
                                        <option>المشاكل الجنسيه</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 mt-3">
                                <img src="{{asset('images/dollar.png')}}" alt="" class="rounded float-right" height="20">
                                <p class="mr-1 float-right">تكلفه الجلسه</p>
                            </div>

                            <div class="col-12 mt-2">
                                <div class="filter-2">
                                    <div class="last-filter">
                                        <div class="filter-price">less than 10</div>
                                        <div class="filter-price">form 10 to 20</div>
                                        <div class="filter-price">form 20 to 30</div>
                                        <div class="filter-price">form 30 to 40</div>
                                        <div class="filter-price">Above 40</div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
