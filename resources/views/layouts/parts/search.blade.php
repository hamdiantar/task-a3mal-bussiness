<div class="row pt-3">
    <div class="col-lg-9 p-3">
        <div class="row">
            <div class="col-4 top-search">
                <form action="">
                    <div class="input-group" style="border: .5px solid #7d7c7c2e;padding: 3px">
                        <select
                            class="form-control border-0 bg-light form-control fz16 sm-no-border-radius h-100 no-border search-bar"
                            id="exampleFormControlSelect1" onchange="sortBy(event)">
                            <option disabled selected>اختر</option>
                            <option value="asc">السعر : من الاقل الى الاعلى</option>
                            <option value="desc">السعر : من الاعلى الى الاقل</option>
                            <option value="rate">الاعلى تقييم</option>
                        </select>
                        <div class="input-group-append" style="background-color: white">
                            <span class="fz16 w110 d-md-block d-none">رتب حسب</span>
                            <span class="mr8">
                                    <svg-icon src="assets/images/search/sort-icon.svg">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="16"
                                             viewBox="0 0 12 16">
                                            <defs><style>.a {
                                                        fill: #666669;
                                                    }</style></defs>
                                            <g transform="translate(-291 -83)">
                                                <path class="a"
                                                      d="M19.8,10.529,17.471,8.2a.667.667,0,0,0-.943,0L14.2,10.529a.667.667,0,1,0,.943.943l1.2-1.2V23.333a.667.667,0,1,0,1.333,0V10.276l1.2,1.2a.667.667,0,1,0,.943-.943Z"
                                                      transform="translate(277 75)"></path>
                                                <path class="a"
                                                      d="M37.8,20.529a.667.667,0,0,0-.943,0l-1.2,1.2V8.667a.667.667,0,0,0-1.333,0V21.724l-1.2-1.2a.667.667,0,0,0-.943.943L34.529,23.8a.666.666,0,0,0,.943,0L37.8,21.471a.667.667,0,0,0,0-.943Z"
                                                      transform="translate(265 75)"></path></g>
                                        </svg></svg-icon>
                                </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-4 top-search d-none show-filter">
                <div class="form-group" style="border: .5px solid #7d7c7c2e; padding: 6px;background-color: white">
                    <button  data-toggle="modal" data-target="#modal_aside_filter" class="btn-filter" type="button"> التصنيف  </button>
                </div>
            </div>
            <div class="col-4 top-search hide-stepper">
                <div class="form-group" style="border: .5px solid #7d7c7c2e;padding: 3px">
                    <select
                        class="form-control border-0 bg-light form-control fz16 sm-no-border-radius h-100 no-border search-bar"
                        id="exampleFormControlSelect1" onchange="filterBy('specialization', event)">
                        <option value="all">جميع التخصصات</option>
                        <option>مشاكل الاطفال</option>
                        <option>مشاكل المراهقه</option>
                        <option>الاكتئاب</option>
                        <option>القلق</option>
                        <option>المشاكل الجنسيه</option>
                    </select>
                </div>
            </div>
            <div class="col-4 top-search">
                <form action="">
                    <div class="input-group" style="border: .5px solid #7d7c7c2e">
                        <input onkeyup="filterBy('name', event)" id="theraPistName" type="search" placeholder="بحث بأسم المعالج" aria-describedby="button-addon1"
                               class="form-control border-0 bg-light form-control fz16 sm-no-border-radius h-100 no-border search-bar-no-cursor">
                        <div class="input-group-append" style="background-color: white">
                            <button id="button-addon1" type="submit" class="btn btn-link text-primary"
                                    style="    color: #675d5d9e  !important;"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('layouts.parts.aside-filter')
