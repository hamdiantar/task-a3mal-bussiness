<div class="wrapper header-nav animated slideInDown" style="direction: rtl" id="header-nav">
  <div class="container">
     <nav class="navbar navbar-expand-lg no-p">
    <a class="navbar-brand no-p" href="#"><img src="{{asset('images/logo.png')}}"></a>

    <button type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler no-border">
        <i class="fa fa-bars text-dark"></i>
    </button>

    <div id="navbarText" class="collapse navbar-collapse">
        <div class="row w-100" style="margin-top: 20px !important;">
            <div class="offset-xl-1 col-xl-6 col-lg-7 d-flex align-items-center justify-content-lg-end">
                <ul style="display: flex">
                <li class="no-outline" tabindex="0">المعالجين </li>
                    <li><a>ازاي تحجز جلستك؟ </a></li>
                    <li class="no-outline" tabindex="0">الاختبارات النفسية</li>
                    <li class="no-outline" tabindex="0">المقالات </li>
                </ul>
            </div>

            <div class="col-xl-5 col-lg-5 d-flex align-items-center justify-content-lg-end">
                <ul style="display: flex">
                    <li><a class="text-black no-hover-underline hover-text-orange">تسجيل الدخول</a></li>
                    <li><a class="text-black no-hover-underline hover-text-orange" href="/ar/signup">انشاء حساب</a></li>
                    <li class="no-outline" tabindex="0">انضم لينا كطبيب</li>
                    <li>English<!----><svg-icon src="assets/images/header/world.svg" class="ml5 world-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15.2" height="15.2" viewBox="0 0 15.2 15.2" class="ml5 world-icon"><defs><style>.a{fill:#4d4d4f;stroke:#4d4d4f;stroke-width:0.2px;}</style></defs><path class="a" d="M13.109,12.471a7.48,7.48,0,0,0-.145-10.1h0l0,0A7.479,7.479,0,0,0,7.76.007h0L7.614,0,7.5,0,7.386,0l-.146,0h0a7.479,7.479,0,0,0-5.2,2.36l0,0h0a7.479,7.479,0,0,0-.145,10.1s0,0,0,.007.011.01.016.016a7.483,7.483,0,0,0,5.33,2.5h0l.146,0L7.5,15l.114,0,.146,0h0a7.483,7.483,0,0,0,5.334-2.5s.009-.006.012-.01S13.107,12.474,13.109,12.471ZM.524,7.759h3.1a9.558,9.558,0,0,0,.625,3.16,9.536,9.536,0,0,0-2.1,1.068A6.951,6.951,0,0,1,.524,7.759ZM2.3,2.851a9.513,9.513,0,0,0,2.043,1,9.55,9.55,0,0,0-.711,3.387H.524A6.952,6.952,0,0,1,2.3,2.851Zm12.18,4.391h-3.1a9.55,9.55,0,0,0-.711-3.387,9.513,9.513,0,0,0,2.043-1A6.952,6.952,0,0,1,14.476,7.241ZM7.241,3.87a9.041,9.041,0,0,1-2.191-.332A9.049,9.049,0,0,1,7.241.684Zm0,.517V7.241h-3.1a9.033,9.033,0,0,1,.689-3.226A9.556,9.556,0,0,0,7.241,4.387Zm.517,0a9.548,9.548,0,0,0,2.407-.372,9.033,9.033,0,0,1,.689,3.226h-3.1Zm0-.517V.684A9.049,9.049,0,0,1,9.949,3.538,9.041,9.041,0,0,1,7.759,3.87Zm2.692-.489a9.551,9.551,0,0,0-2.04-2.8,6.963,6.963,0,0,1,3.927,1.893A9,9,0,0,1,10.451,3.381Zm-5.9,0a8.987,8.987,0,0,1-1.886-.91A6.962,6.962,0,0,1,6.59.579,9.547,9.547,0,0,0,4.549,3.381Zm-.4,4.378h3.1v2.6a9.539,9.539,0,0,0-2.5.4A9.032,9.032,0,0,1,4.145,7.759Zm3.1,3.113v3.444a9.044,9.044,0,0,1-2.3-3.082A9.017,9.017,0,0,1,7.241,10.872Zm.517,3.444V10.872a9.017,9.017,0,0,1,2.3.362A9.043,9.043,0,0,1,7.759,14.316Zm0-3.962v-2.6h3.1a9.029,9.029,0,0,1-.6,2.995A9.525,9.525,0,0,0,7.759,10.355Zm3.613-2.6h3.1a6.949,6.949,0,0,1-1.631,4.228,9.536,9.536,0,0,0-2.1-1.068A9.551,9.551,0,0,0,11.372,7.759ZM2.506,12.374A9.013,9.013,0,0,1,4.45,11.4a9.543,9.543,0,0,0,2.139,3.025A6.968,6.968,0,0,1,2.506,12.374Zm5.9,2.047A9.537,9.537,0,0,0,10.55,11.4a9.019,9.019,0,0,1,1.944.978A6.968,6.968,0,0,1,8.411,14.421Z" transform="translate(0.1 0.1)"></path></svg></svg-icon></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
  </div>
</div>
