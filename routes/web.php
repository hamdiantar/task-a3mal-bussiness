<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'TherapistController@getAll');
Route::post('/therapist/filter', 'TherapistController@filter')->name('therapist.filter');
Route::post('/therapist/sort', 'TherapistController@sort')->name('therapist.sort');
