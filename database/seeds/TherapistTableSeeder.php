<?php

use App\Therapist;
use Illuminate\Database\Seeder;

class TherapistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 50;
        factory(Therapist::class, $count)->create();
    }
}
