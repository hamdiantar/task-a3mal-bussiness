<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Therapist;
use Faker\Generator as Faker;

$factory->define(Therapist::class, function (Faker $faker) {

    $spec = Therapist::getListSpecialization();
    return [
        'name' => $faker->name,
        'price' => random_int(100,1000),
        'specialization' => $spec[rand(0,4)],
        'rate' => rand(1,5),
    ];
});
